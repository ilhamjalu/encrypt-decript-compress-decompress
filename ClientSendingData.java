/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientsendingdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.DeflaterOutputStream;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Aleksis Xancez
 */
public class ClientSendingData {

    /**
     * @param args the command line arguments
     */
    
	private Socket s;
        static File file;
        private static SecretKeySpec secretKey;
        private static byte[] key;
        private static final String ALGORITHM = "AES";
        private static final String TRANSFORMATION = "AES";
	
        
	public ClientSendingData(String host, int port, File file) throws Exception{
		try {
                        file = new File("pic.jpg");
			s = new Socket(host, port);
			sendFile(file);
		} catch (Exception e) {
			System.out.print("selesai");
		}		
	}
	
	public void sendFile(File file) throws Exception {
            FileInputStream fis2=new FileInputStream(file); 
            FileOutputStream fos=new FileOutputStream("compress.rar"); 
            DeflaterOutputStream des=new DeflaterOutputStream(fos); 
  
       
            int data; 
            while ((data=fis2.read())!=-1) 
            { 
                des.write(data); 
            } 
  
            //close the file 
            fis2.close(); 
            des.close(); 
        
                //key
             String key = "abcde";
            File inputFile = new File("compress.rar");
            File encryptedFile = new File("compress.encrypted");
         
            try {
                ClientSendingData.encrypt(key, inputFile, encryptedFile);
            } catch (CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            }
                
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            FileInputStream fis = new FileInputStream(encryptedFile);
            byte[] buffer = new byte[4096];
		
            while (fis.read(buffer) > 0) {
		dos.write(buffer);
            }
		
            fis.close();
            dos.close();	
	}
        
        public static void encrypt(String key, File inputFile, File outputFile) throws Exception {
        doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }
        private static void doCrypto(int cipherMode, String key, File inputFile,
            File outputFile) throws Exception {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
             
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
             
            byte[] outputBytes = cipher.doFinal(inputBytes);
             
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
             
            inputStream.close();
            outputStream.close();
             
        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | BadPaddingException
                | IllegalBlockSizeException | IOException ex) {
            throw new CryptoException("Error encrypting", ex);
        }
    }
        
	public static void main(String[] args) throws Exception {
		ClientSendingData fc = new ClientSendingData("localhost", 1988, file);
	}
            public static class CryptoException extends Exception {
 
    public CryptoException() {
    }
 
    public CryptoException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
}

